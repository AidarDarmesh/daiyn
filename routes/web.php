<?php

Auth::routes();

// Admin routes
Route::get('/home', 'HomeController@index');

// Clients
Route::get('/clients', 'ClientController@clients');
Route::get('/clients/new', 'ClientController@new');
Route::post('/clients/store', 'ClientController@store');
Route::get('/clients/{client}', 'ClientController@details');
Route::get('/clients/{client}/edit', 'ClientController@editView');
Route::post('/clients/{client}/edit', 'ClientController@edit');
Route::get('/clients/{client}/del', 'ClientController@del');

// Orders
Route::get('/orders', 'OrderController@orders');
Route::get('/orders/new', 'OrderController@new');
Route::post('/orders/store', 'OrderController@store');
Route::get('/orders/{order}', 'OrderController@details');
Route::get('/orders/{order}/edit', 'OrderController@editView');
Route::post('/orders/{order}/edit', 'OrderController@edit');
Route::get('/orders/{order}/del', 'OrderController@del');

// Terminals
Route::get('/terminals', 'TerminalController@list');
Route::get('/terminals/new', 'TerminalController@new');
Route::post('/terminals/store', 'TerminalController@store');
Route::get('/terminals/{terminal}', 'TerminalController@details');
Route::get('/terminals/{terminal}/edit', 'TerminalController@editView');
Route::post('/terminals/{terminal}/edit', 'TerminalController@edit');
Route::get('/terminals/{terminal}/del', 'TerminalController@del');

// Bots routes
Route::post('/fX5bTv9JCrRukHUZ/vk_bot', 'VkBotController@messageNew');
Route::post('/fX5bTv9JCrRukHUZ/tg_bot', 'TgBotController@update');

// Printing routes
Route::post('orders/get', 'TerminalController@get');
Route::post('orders/printed', 'TerminalController@printed');

// Payment routes
Route::get('/pay', 'PayController@index');
Route::get('/link', 'PayController@link');
Route::get('/pay/{order_id}/{price}', 'PayController@pay');