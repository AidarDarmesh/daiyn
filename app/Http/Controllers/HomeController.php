<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\Order;
use App\Terminal;

class HomeController extends Controller
{

    public function __construct()
    {

        $this->middleware('auth');

    }

    public function index()
    {

        $clients_numb = Client::all()->count();
        $orders_numb = Order::all()->count();
        $terminals_numb = Terminal::all()->count();        

        return view('home', [
                            'clients_numb' => $clients_numb,
                            'orders_numb' => $orders_numb,
                            'terminals_numb' => $terminals_numb,
                            ]);

    }

}
