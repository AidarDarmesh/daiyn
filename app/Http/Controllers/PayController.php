<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

require_once('../public/paysys/kkb.utils.php');

class PayController extends Controller
{
    
	public function index()
	{

		$config_path = "paysys/config.txt";
		$currency_id = "398";
		$order_id = 407126;
		$price = 20;

		// MAKING SIGNED_ORDER
		$content = process_request($order_id, $currency_id, $price, $config_path);

		return $content;

	}

	public function link()
	{

	    $chat_id = 236024799;
	    $url = 'http://127.0.0.1:8000/pay/1/20';
	    $text = $url;
	    $api = 'https://api.telegram.org/bot' . config('tg.token') . '/sendMessage?chat_id=' . $chat_id . '&text=' . urlencode($text);
	    file_get_contents($api);

	}

	public function pay($order_id, $price)
	{

		$config_path = "paysys/config.txt";
		$currency_id = "398";

		// MAKING SIGNED_ORDER
		$signed_order = process_request($order_id, $currency_id, $price, $config_path);

		$options = [
			'Signed_Order_B64' => $signed_order,
			'email' => 'darmesh.aidar@gmail.com',
			'Language' => 'rus',
			'BackLink' => 'http://127.0.0.1:8000/back',
			'FailureBackLink' => 'http://127.0.0.1:8000/fail',
			'PostLink' => 'http://127.0.0.1:8000/postlink',
			'FailurePostLink' => 'http://127.0.0.1:8000/fail_postlink'
		];

		$url = 'https://epay.kkb.kz/jsp/process/logon.jsp';

		$client = new \GuzzleHttp\Client();
		$response = $client->request('POST', $url, ['form_params' => $options]);

		return $response->getBody();

	}

}
