<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Terminal;
use App\Order;

class TerminalController extends Controller
{
    
	public function get(Request $request)
	{

		$info = $request->all();

		// Если такой терминал есть в системе
		if( Terminal::find($info['id']) !== null )
		{

			$order = Order::where('numb', $info['numb'])->first();

			// Если такой заказ существует
			if( $order !== null )
			{

				return $order;

			} else {

				return "Такого заказ нет";

			}

		} else {

			return "Такого терминала нет в системе";

		}

	}

	public function list()
	{

		$terminals = Terminal::all();

		return view('terminals.list', [
								'terminals' => $terminals
								]);

	}

	public function new()
	{

		return view('terminals.new');

	}

	public function store(Request $request)
	{
		
		$info = $request->all();

		$terminal = new Terminal();

		$terminal->id = $info['id'];
		$terminal->phone = $info['phone'];
		$terminal->traffic = $info['traffic'];
		$terminal->pages = $info['pages'];
		$terminal->toner = $info['toner'];
		$terminal->desc = $info['desc'];

		$terminal->save();

		return redirect('terminals');

	}

	public function details(Terminal $terminal)
	{

		return view('terminals.details', [
								'terminal' => $terminal
								]);

	}

	public function editView(Terminal $terminal)
	{

		return view('terminals.edit', [
							'terminal' => $terminal
							]);

	}

	public function edit(Request $request)
	{

		$info = $request->all();

		$terminal = Terminal::where('id', $info['id'])
							->update([
								'id' => $info['id'],
								'phone'  => $info['phone'],
								'traffic' => $info['traffic'],
								'pages' => $info['pages'],
								'toner' => $info['toner'],
								'done' => $info['done'],
								'desc' => $info['desc']
							]);

		return redirect('terminals');

	}

	public function del(Terminal $terminal)
	{

		$terminal->delete();

		return redirect('terminals');

	}

}
