<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PDFController extends Controller
{
    
	public static function pdf($url, $numb)
	{

		// Имя загрузочного файла
		$hash_name = $numb . ".pdf";

		// Если файл был загружен на сервер
		if( copy($url, $hash_name) )
		{

			// Выполняю скрипт и делаю парсинг на то, что выйдет в консоли
	        // Никаких знаков в Windows и обязательный ./ в Linux
	        exec("pdfinfo {$hash_name}", $output);

	        // Бегаем по строкам
	        $pages = 0;

	        foreach($output as $op){
	            // Вытаскиваем число
	            if(preg_match("/Pages:\s*(\d+)/i", $op, $matches) === 1){
	                // Кол-во страниц
	                $pages = intval($matches[1]);
	                break;
	            }
	        }

	        // Удаляю файл, чтобы не занимать много места на сервере
			unlink($hash_name);

	        return $pages;

		} else {

			return;

		}

	}

}
