<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\HandleTgUpdate;

class TgBotController extends Controller
{
    
	public function update(Request $request)
	{

		// Получаем и декодируем уведомление
		$data = json_decode($request->getContent());

		dispatch(new HandleTgUpdate($data));

	}

}
