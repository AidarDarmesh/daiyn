<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;

class ClientController extends Controller
{

	public function clients()
    {

        $clients = Client::all();

        return view('clients.list', [
                            'clients' => $clients
                            ]);

    }

	public function new()
	{

		return view('clients.new');

	}

	public function store(Request $request)
	{
		
		$info = $request->all();

		$client = new Client();

		$client->id = $info['id'];
		$client->from = $info['from'];
		$client->first_name = $info['first_name'];
		$client->last_name = $info['last_name'];
		$client->username = $info['username'];

		$client->save();

		return redirect('clients');

	}

	public function details(Client $client)
	{

		return view('clients.details', [
								'client' => $client
								]);

	}

	public function editView(Client $client)
	{

		return view('clients.edit', [
							'client' => $client
							]);

	}

	public function edit(Request $request)
	{

		$info = $request->all();

		$terminal = Client::where('id', $info['id'])
							->update([
								'id' => $info['id'],
								'from'  => $info['from'],
								'first_name' => $info['first_name'],
								'last_name' => $info['last_name'],
								'username' => $info['username']
							]);

		return redirect('clients');

	}

	public function del(Client $client)
	{

		$client->delete();

		return redirect('clients');

	}

    public static function signUpVk($user_id)
    {

    	// Если клиента нет в системе, зарегай его
		if( Client::find('vk' . $user_id) === null )
		{
			$about = json_decode(file_get_contents("https://api.vk.com/method/users.get?user_ids=" . $user_id . "&fields=domain" . "&v=" . config('vk.v')));

			$client = new Client();
			$client->id = 'vk' . $about->response[0]->id;
			$client->from = 'vk';
			$client->first_name = $about->response[0]->first_name;
			$client->last_name = $about->response[0]->last_name;
			$client->username = $about->response[0]->domain;
			$client->save();

		}

		return;

    }

    public static function signUpTg($from)
    {

    	// Если клиента нет в системе, зарегай его
		if( Client::find('tg' . $from->id) === null )
		{
			
			$client = new Client();
			$client->id = 'tg' . $from->id;
			$client->from = 'tg';
			$client->first_name = $from->first_name;
			$client->last_name = $from->last_name;
			$client->username = $from->username;
			$client->save();

		}

    }

    public static function replyVk($reply, $user_id)
    {

    	$request_params = array(
            "message" => $reply,
            "user_id" => $user_id,
            "access_token" => config("vk.token"),
            "v" => config("vk.v")
        );

        $http_params = http_build_query($request_params);

        file_get_contents("https://api.vk.com/method/messages.send?" . $http_params);

    }

    public static function replyTg($chat_id, $text)
    {

	    $api = 'https://api.telegram.org/bot' . config('tg.token') . '/sendMessage?chat_id=' . $chat_id . '&text=' . urlencode($text);
	    file_get_contents($api);

    }
    
}
