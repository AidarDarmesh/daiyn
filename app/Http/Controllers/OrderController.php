<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Terminal;

class OrderController extends Controller
{
    
	public function orders()
    {

        $orders = Order::all();

        return view('orders.list', [
                            'orders' => $orders
                            ]);

    }

    public function new()
	{

		return view('orders.new');

	}

	public function store(Request $request)
	{
		
		$info = $request->all();

		$order = new Order();

		$order->numb = $info['numb'];
		$order->client_id = $info['client_id'];
		$order->file_name = $info['file_name'];
		$order->url = $info['url'];
		$order->pages = $info['pages'];
		$order->size = $info['size'];

		$order->save();

		return redirect('orders');

	}

	public function details(Order $order)
	{

		return view('orders.details', [
								'order' => $order
								]);

	}

	public function editView(Order $order)
	{

		return view('orders.edit', [
							'order' => $order
							]);

	}

	public function edit(Request $request)
	{

		$info = $request->all();

		$order = Order::where('numb', $info['numb'])
							->update([
								'numb' => $info['numb'],
								'client_id'  => $info['client_id'],
								'file_name' => $info['file_name'],
								'url' => $info['url'],
								'pages' => $info['pages'],
								'size' => $info['size']
							]);

		return redirect('orders');

	}

	public function del(Order $order)
	{

		$order->delete();

		return redirect('orders');

	}

	public static function orderAndReplyVk($object)
	{

		// Проверяем, существуют ли вложенности
        if( property_exists($object, "attachments") ){

        	// Считаю кол-во элементов во вложенности
            $numb_elems = count($object->attachments);

            // Мне нужен только 1 элемент
            if( $numb_elems == 1 )
			{

				// Делаю проверку на тип вложенности этого элемента мне нужен только doc
				if( property_exists($object->attachments[0], "doc") )
				{

					// Сделаем отдельный объект doc для удобства ссылания
					$doc = $object->attachments[0]->doc;

					// Проверяю тип документа, беру только pdf
					if( $doc->ext == "pdf" )
					{

						// Создадим номер заказа
						$numb = rand(100000, 999999);

						// Обрабатываем pdf и возвращаем число страниц
						$pages = PDFController::pdf($doc->url, $numb);

						// Записываю заказ в БД и отправляю ответ
						$order = new Order();
						$order->client_id = 'vk' . $object->user_id;
						$order->numb = $numb;
						$order->file_name = $doc->title;
						$order->url = $doc->url;
						$order->pages = $pages;
						$order->size = $doc->size/pow(1024, 2);

						if( $order->save() )
						{

							return  "Номер заказа {$numb}\n" . 
									"{$doc->title}\n" . 
									"{$pages} стр";

						} else{

							return "Ошибка при оформлении заказа";

						}

					} else {


						return "Вы прислали файл НЕ PDF, пришлите PDF";

					}

				} else {

					return "Ваш файл не является документом";

				}

			} else {

				return "Вы прислали НЕСКОЛЬКО файлов, а нужно ОДИН";

			}

        } else {

            return "Вы НЕ прислали файл";
                
        }

	}

	public static function orderAndReplyTg($doc, $client_id)
	{

		// Если пдф, продолжаем работу
		if( $doc->mime_type === 'application/pdf' )
		{

			// Получить доп данные о файле
			$file_api = 'https://api.telegram.org/bot' . config('tg.token') . '/getFile?file_id=' . $doc->file_id;
			$info = json_decode(file_get_contents($file_api));

			// Скачиваем файл
			if( $info->ok )
			{

				// Выберем имя файла
				$numb = rand(100000, 999999);

				// Ссылка на скачивание файла
				$url = 'https://api.telegram.org/file/bot' . config('tg.token') . '/' . $info->result->file_path;

				// Обрабатываем pdf и возвращаем число страниц
				$pages = PDFController::pdf($url, $numb);

				// Записываю заказ в БД и отправляю ответ
				$order = new Order();
				$order->client_id = $client_id;
				$order->numb = $numb;
				$order->file_name = $doc->file_name;
				$order->url = $url;
				$order->pages = $pages;
				$order->size = $doc->file_size/pow(1024, 2);

				if( $order->save() )
				{

					return  "Номер заказа {$numb}\n" . 
							"{$doc->file_name}\n" . 
							"{$pages} стр";

				} else{

					return "Ошибка при оформлении заказа";

				}

			} else {

				return 'Не удалось найти ваш файл';

			}

		} else {

			return 'Мне нужен PDF...';

		}

	}

}
