<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{

    public $incrementing = false;

    public function orders()
    {

    	return $this->hasMany(Order::class);

    }

}
