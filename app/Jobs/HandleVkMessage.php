<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Http\Controllers\ClientController;
use App\Http\Controllers\OrderController;

class HandleVkMessage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct($data)
    {
        
        // Регистрируем клиента
        ClientController::signUpVk($data->object->user_id);

        // Оформи заказ и дай мне обратный ответ
        $reply = OrderController::orderAndReplyVk($data->object);

        // Отправляем ответное сообщение
        ClientController::replyVk($reply, $data->object->user_id);

    }

    public function handle()
    {
        


    }
}
