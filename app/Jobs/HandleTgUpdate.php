<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Http\Controllers\ClientController;
use App\Http\Controllers\OrderController;

class HandleTgUpdate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct($data)
    {

    	// Регистрируем клиента
        ClientController::signUpTg($data->message->from);
        
        // Если это текстовая команда
		if( property_exists($data->message, 'text') )
		{

			// Если это начало работы с ботом
			if($data->message->text === '/start')
			{

				// Welcome сообщение
	            $text = 'Привет, я бот qagaz daiyn, вышли мне 1 pdf файл';
	            ClientController::replyTg($data->message->chat->id, $text);

			} else {

				// Если это другие простые текстовые сообщения
	            $text = 'Классная история! А теперь пришлите мне только один PDF файл';
	            ClientController::replyTg($data->message->chat->id, $text);

			}

		} else {

			// Если прислали не текст, то проси документ
			if( property_exists($data->message, "document") )
			{

				$text = OrderController::orderAndReplyTg($data->message->document, 'tg' . $data->message->from->id);

				// Если это другие медиа, не текст и не документ
	            ClientController::replyTg($data->message->chat->id, $text);

			} else {

				// Если это другие медиа, не текст и не документ
	            $text = 'Принимаю только файл)';
	            ClientController::replyTg($data->message->chat->id, $text);

			}

		}

    }

    public function handle()
    {
        


    }
}
