<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Terminal extends Model
{
    
    public $incrementing = false;

    public function editPath()
    {

    	return $this->path();

    }

}
