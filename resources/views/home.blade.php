@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Statistics</div>
                <div class="panel-body">
                    <p>
                        <a href="{{ url('/clients') }}">Clients: {{ $clients_numb }}</a>
                    </p>
                    <p>
                        <a href="{{ url('/orders') }}">Orders: {{ $orders_numb }}</a>
                    </p>
                    <p>
                        <a href="{{ url('/terminals') }}">Terminals: {{ $terminals_numb }}</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
