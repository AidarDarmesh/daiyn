@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">New order</div>

                <div class="panel-body">
                    <form method="POST" action="/orders/store">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <p>
                                <label for="numb">Numb*</label>
                                <input id="numb" type="text" name="numb" class="form-control" required="on" value="{{ rand(100000, 999999) }}">
                            </p>
                            <p>
                                <label for="client_id">Client Id*</label>
                                <input id="client_id" type="text" name="client_id" class="form-control" required="on" placeholder="tg236024799" value="tg{{ rand(100000, 999999) }}">
                            </p>
                            <p>
                                <label for="file_name">File name*</label>
                                <input id="file_name" type="text" name="file_name" class="form-control" required="on" placeholder="MegaSuperFile.pdf" value="MegaSuperFile.pdf">
                            </p>
                            <p>
                                <label for="url">Url*</label>
                                <input id="url" type="text" name="url" class="form-control" required="on" placeholder="https://vk.com/...." value="https://vk.com/....">
                            </p>
                            <p>
                                <label for="pages">Pages*</label>
                                <input id="pages" type="text" name="pages" class="form-control" required="on" placeholder="qagaz_bot" value="1">
                            </p>
                            <p>
                                <label for="size">Size*</label>
                                <input id="size" type="text" name="size" class="form-control" required="on" value="1.67">
                            </p>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
