@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">Edit order</div>

                <div class="panel-body">
                    @if($order)
                    <form method="POST" action="/orders/{{ $order->id }}/edit">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <p>
                                <label for="numb">Numb*</label>
                                <input id="numb" type="text" name="numb" class="form-control" required="on" value="{{ $order->numb }}">
                            </p>
                            <p>
                                <label for="client_id">Client Id*</label>
                                <input id="client_id" type="text" name="client_id" class="form-control" required="on" placeholder="tg236024799" value="{{ $order->client_id }}">
                            </p>
                            <p>
                                <label for="file_name">File name*</label>
                                <input id="file_name" type="text" name="file_name" class="form-control" required="on" placeholder="MegaSuperFile.pdf" value="{{ $order->file_name }}">
                            </p>
                            <p>
                                <label for="url">Url*</label>
                                <input id="url" type="text" name="url" class="form-control" required="on" placeholder="https://vk.com/...." value="{{ $order->url }}">
                            </p>
                            <p>
                                <label for="pages">Pages*</label>
                                <input id="pages" type="text" name="pages" class="form-control" required="on" placeholder="qagaz_bot" value="{{ $order->pages }}">
                            </p>
                            <p>
                                <label for="size">Size*</label>
                                <input id="size" type="text" name="size" class="form-control" required="on" value="{{ $order->size }}">
                            </p>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                    @else
                    <p>Nothing found</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
