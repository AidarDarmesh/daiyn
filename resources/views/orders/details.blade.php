@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">Order {{ $order->id }}</div>

                <div class="panel-body">
                    <p>Numb: {{ $order->numb }}</p>
                    <p>File name: {{ $order->file_name }}</p>
                    <p>Url: <a href="{{ $order->url }}">url</a></p>
                    <p>Pages: {{ $order->pages }}</p>
                    <p>Size: {{ $order->size }}</p>
                    <p>Printed: {{ $order->printed_at }}</p>
                    <p>Created: {{ $order->created_at }}</p>
                    <p>Updated: {{ $order->updated_at }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
