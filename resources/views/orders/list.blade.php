@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Orders <a href="/orders/new" class="btn btn-success">new</a></div>

                <div class="panel-body">
                    @if( sizeof($orders) === 0)
                        <p>You don't have orders yet!</p>
                    @else
                        <table class="table table-striped table-bordered">
                            <tr>
                                <th>numb</th>
                                <th>client_id</th>
                                <th>file name</th>
                                <th>pages</th>
                                <th>size</th>
                                <th>actions</th>
                            </tr>
                            @foreach($orders as $order)
                                <tr>
                                    <td>
                                        <a href="/orders/{{ $order->id }}">{{ $order->numb }}</a>
                                    </td>
                                    <td>{{ $order->client_id }}</td>
                                    <td>{{ $order->file_name }}</td>
                                    <td>{{ $order->pages }}</td>
                                    <td>{{ $order->size }}</td>
                                    <td>
                                        <a href="/orders/{{ $order->id }}/edit" class="btn btn-info">edit</a>
                                        <a href="/orders/{{ $order->id }}/del" class="btn btn-danger">del</a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
