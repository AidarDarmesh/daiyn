@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">Terminal {{ $terminal->id }}</div>

                <div class="panel-body">
                    <p>Description: {{ $terminal->desc }}</p>
                    <p>Pages: {{ $terminal->pages }}</p>
                    <p>Toner: {{ $terminal->toner }}</p>
                    <p>Done: {{ $terminal->done }}</p>
                    <p>Created: {{ $terminal->created_at }}</p>
                    <p>Updated: {{ $terminal->updated_at }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
