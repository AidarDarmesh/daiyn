@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">Edit client</div>

                <div class="panel-body">
                    @if($client)
                    <form method="POST" action="/clients/{{ $client->id }}/edit">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <p>
                                <label for="id">Id*</label>
                                <input id="id" type="text" name="id" class="form-control" required="on" value="{{ $client->id }}">
                            </p>
                            <p>
                                <label for="from">From*</label>
                                <input id="from" type="text" name="from" class="form-control" required="on" placeholder="+77086273347" value="{{ $client->from }}">
                            </p>
                            <p>
                                <label for="first_name">First name*</label>
                                <input id="first_name" type="text" name="first_name" class="form-control" required="on" value="{{ $client->first_name }}">
                            </p>
                            <p>
                                <label for="last_name">Last name*</label>
                                <input id="last_name" type="text" name="last_name" class="form-control" required="on" value="{{ $client->last_name }}">
                            </p>
                            <p>
                                <label for="username">Username*</label>
                                <input id="username" type="text" name="username" class="form-control" required="on" value="{{ $client->username }}">
                            </p>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                    @else
                    <p>Nothing found</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
