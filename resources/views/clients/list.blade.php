@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Clients <a href="/clients/new" class="btn btn-success">new</a></div>

                <div class="panel-body">
                    @if( sizeof($clients) === 0)
                        <p>You don't have clients yet!</p>
                    @else
                        <table class="table table-striped table-bordered">
                            <tr>
                                <th>id</th>
                                <th>first name</th>
                                <th>last name</th>
                                <th>username</th>
                                <th>actions</th>
                            </tr>
                            @foreach($clients as $client)
                                <tr>
                                    <td>
                                        <a href="/clients/{{ $client->id }}">{{ $client->id }}</a>
                                    </td>
                                    <td>{{ $client->first_name }}</td>
                                    <td>{{ $client->last_name }}</td>
                                    <td>{{ $client->username }}</td>
                                    <td>
                                        <a href="/clients/{{ $client->id }}/edit" class="btn btn-info">edit</a>
                                        <a href="/clients/{{ $client->id }}/del" class="btn btn-danger">del</a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
