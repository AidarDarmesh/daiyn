@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">Client {{ $client->id }}</div>

                <div class="panel-body">
                    <p>From: {{ $client->from }}</p>
                    <p>First name: {{ $client->first_name }}</p>
                    <p>Last name: {{ $client->last_name }}</p>
                    @if( $client->from === 'vk' )
                        <p>Username: <a href="https://vk.com/{{ $client->username }}">{{ $client->username }}</a></p>
                    @else if()
                        <p>Username: <a href="https://t.me/{{ $client->username }}">{{ $client->username }}</a></p>
                    @endif
                    <p>Created: {{ $client->created_at }}</p>
                    <p>Updated: {{ $client->updated_at }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
