@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Orders</div>

                <div class="panel-body">
                    @if( sizeof($orders) === 0)
                        <p>You don't have orders yet!</p>
                    @else
                        <table class="table table-striped table-bordered">
                            <tr>
                                <th>id</th>
                                <th>numb</th>
                                <th>file name</th>
                                <th>pages</th>
                                <th>created</th>
                            </tr>
                            @foreach($orders as $order)
                                <tr>
                                    <td>{{ $order->id }}</td>
                                    <td>{{ $order->numb }}</td>
                                    <td>
                                        <a href="{{ $order->url }}">{{ $order->file_name }}</a>
                                    </td>
                                    <td>{{ $order->pages }}</td>
                                    <td>{{ $order->created_at }}</td>
                                </tr>
                            @endforeach
                        </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
